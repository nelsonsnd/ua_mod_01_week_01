Guías prácticas del Módulo 1:

a. diseñar e implementar ese diseño de un sitio web responsive utilizando NPM,
b. configurar tu repositorio utilizando Git y Bitbucket e
c. implementar sistemas de grillas de Bootstrap.

En tu proyecto deberá verse:

1. el archivo .gitignore para evitar hacer seguimiento de los cambios en el directorio node_modules.
2. el script dev sobre el package.json que debe inicializar el server lite-server
3. las referencias css y js a bootstrap, jquery y popper utilizando referencias a los archivos correspondientes de la carpeta node_modules sobre la página principal (index.html) , en el body, un elemento jumbotron con el título de su proyecto.
4. un container con 3 párrafos utilizando los elementos p y un título con descripción a cada uno contando las principales características del producto.
5. dentro del container y separado de los párrafos previos, una lista de productos que incluya un título, descripción y un botón, esta vez utilizando el sistema de grillas con filas y columnas. Y todo esto, ¿es responsive?
6. el sistema de grillas donde ubicó los productos transformado a flex
7. una sección de footer en la página principal que incluye información del comercio, redes sociales y algún otro detalle referido al sitio.
8. En el caso de la dirección comercial utilizando el componente address
9. en el archivo css creado, utiliza estilos que sean replicables para todos los productos del listado principal, de forma tal de cambiar ese diseño en el css y cambien todos los productos?
10. las clases de control de alineación para ubicar los elementos del footer a la izquierda, centro y derecha

Recuerda que esta actividad será evaluada por tus pares y se espera que también lo hagas tú. Por ello, es muy importante que evalúes a conciencia, pensando en que tantos tus compañeros como vos, están queriendo aprender en este curso. Guíate por los criterios de corrección que te orientarán en todo el proceso de evaluación.

Deberás proporcionar la dirección (URL) del repositorio de código de tu proyecto.

Se evaluará tu entrega en base a los siguientes criterios:
1. Completitud: se evaluará si has realizado la consigna de forma parcial o total.
2. Evidencias de aprendizaje: se evaluará el grado de elaboración propia en cada consigna, así como el uso de los contenidos teóricos del módulo en el desarrollo de las respuestas.

Finalmente, habrá un espacio para que tus pares dejen comentarios que enriquezcan la corrección y que profundicen en la misma. Esta última sección es opcional.